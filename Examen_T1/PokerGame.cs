﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_T1
{
    public class PokerGame
    {
        public string VerificarJugadores(int g)
        {
            if (g >= 2 && g <= 5)
            {
                return "Los jugadores son los adecuados";
            }
            else
            {
                throw new Exception("No cumplen con las reglas de jugadores");
            }
        }
        public string Ganador(Jugador[] jugadores)
        {
            string texto = "";
            Jugador temp = new Jugador();
            for (int i = 1; i < jugadores.Length; i++)
            {
                for (int j = jugadores.Length - 1; j >= i; j--)
                {
                    if (jugadores[j - 1].puntaje > jugadores[j].puntaje)
                    {
                        temp = jugadores[j - 1];
                        jugadores[j - 1] = jugadores[j];
                        jugadores[j] = temp;
                    }
                }
            }
            int k = 0;
            if (jugadores[k].puntaje == jugadores[k + 1].puntaje)
            {
                texto = "Empate entre: " + jugadores[k].nombre + " con " + jugadores[k].puntaje + " y " + jugadores[k + 1].nombre + " con " + jugadores[k + 1].puntaje;
                return texto;
            }
            else
            {
                texto = "El ganador es: " + jugadores[k].nombre + " con " + jugadores[k].puntaje;
                return texto;
            }
        }
        public string CartasRepetidas(Carta[]cartas)
        {
            for (int i = 0; i < cartas.Length; i++)
            {
                if (cartas[i].mazo == cartas[i].mazo)
                {
                    if (cartas[i].numero == cartas[i].numero)
                    {
                        return "No hay cartas repetidas";
                    }
                }
            }
            throw new Exception("Existen cartas repetidas");
        }
        public string Todo(Jugador[] jugadores)
        {
            string texto = "";
            int n = jugadores.Length;
            string ju = VerificarJugadores(n);
            if(ju.Equals("Los jugadores son los adecuados"))
            {
                texto = Ganador(jugadores);

                return texto;
            }
            throw new Exception("No cumplen con las reglas de jugadores");
        }
    }
}
