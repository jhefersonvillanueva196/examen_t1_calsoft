﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_T1.Test
{
    class PruebasUnitarias
    {
        [Test]
        public void Caso1()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso2()
        {
            var utils = new PokerGame();
            Assert.Throws(typeof(Exception), () => utils.VerificarJugadores(1));
        }
        [Test]
        public void Caso3()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(2);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso4()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(5);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso5()
        {
            var utils = new PokerGame();
            Assert.Throws(typeof(Exception), () => utils.VerificarJugadores(6));
        }
        [Test]
        public void Caso6()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso7()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 6 },
            new Jugador { nombre = "Pablo", puntaje = 5 },
            new Jugador { nombre = "Juan", puntaje = 1 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Juan con 1", resultado);
            //Los numeros simulan el valor que se dan en las reglas (1 => Escalera Real)
        }
        [Test]
        public void Caso8()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 5 },
            new Jugador { nombre = "Pablo", puntaje = 5 },
            new Jugador { nombre = "Miguel", puntaje = 2 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Miguel con 2", resultado);
        }
        [Test]
        public void Caso9()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 7 },
            new Jugador { nombre = "Pablo", puntaje = 2 },
            new Jugador { nombre = "Miguel", puntaje = 9 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Pablo con 2", resultado);
        }
        [Test]
        public void Caso10()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 8 },
            new Jugador { nombre = "Rodrigo", puntaje = 3 },
            new Jugador { nombre = "Manuel", puntaje = 5 },
            new Jugador { nombre = "Alex", puntaje = 8 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Rodrigo con 3", resultado);
        }
        [Test]
        public void Caso11()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 8 },
            new Jugador { nombre = "Rodrigo", puntaje = 3 },
            new Jugador { nombre = "Manuel", puntaje = 5 },
            new Jugador { nombre = "Alex", puntaje = 8 },
        };
            var paso = jugadores.ToArray().Length;
            var resultado = utils.VerificarJugadores(paso);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso12()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Felix", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Pablo con 1", resultado);
        }
        [Test]
        public void Caso13()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 8 },
            new Jugador { nombre = "Rodrigo", puntaje = 3 },
            new Jugador { nombre = "Manuel", puntaje = 5 },
            new Jugador { nombre = "Alex", puntaje = 8 },
            new Jugador { nombre = "Alex", puntaje = 6 },
        };
            var paso = jugadores.ToArray().Length;
            Assert.Throws(typeof(Exception), () => utils.VerificarJugadores(paso));
        }
        [Test]
        public void Caso14()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 8 },
            new Jugador { nombre = "Pablo", puntaje = 2 },
            new Jugador { nombre = "Miguel", puntaje = 2 },
            new Jugador { nombre = "Andres", puntaje = 11 },
            new Jugador { nombre = "Juan", puntaje = 11 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("Empate entre: Pablo con 2 y Miguel con 2", resultado);
        }
        [Test]
        //por arreglar
        public void Caso15()
        {
            var utils = new PokerGame();
            List<Carta> cartas = new List<Carta>() { new Carta { mazo = "Cocos", numero = 8 },
            new Carta { mazo = "Espadas", numero = 7 },
            new Carta { mazo = "Corazones", numero = 7 },
            new Carta { mazo = "Espadas", numero = 1 },
            new Carta { mazo = "Trevol", numero = 12 },
        };
            var paso = cartas.ToArray();
            var resultado = utils.CartasRepetidas(paso);
            Assert.AreEqual("No hay cartas repetidas", resultado);
        }
        [Test]
        public void Caso16()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 8 },
            new Jugador { nombre = "Pablo", puntaje = 2 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Andres", puntaje = 2 },
            new Jugador { nombre = "Juan", puntaje = 11 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("Empate entre: Pablo con 2 y Andres con 2", resultado);
        }
        [Test]
        public void Caso17()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 8 },
            new Jugador { nombre = "Pablo", puntaje = 5 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Andres", puntaje = 5 },
            new Jugador { nombre = "Juan", puntaje = 10 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("Empate entre: Pablo con 5 y Andres con 5", resultado);
        }
        [Test]
        public void Caso18()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 8 },
            new Jugador { nombre = "Pablo", puntaje = 5 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Andres", puntaje = 5 },
            new Jugador { nombre = "Juan", puntaje = 1 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Ganador(paso);
            Assert.AreEqual("El ganador es: Juan con 1", resultado);
        }
        [Test]
        public void Caso19()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Felix", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("El ganador es: Pablo con 1", resultado);
        }
        [Test]
        public void Caso20()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Felix", puntaje = 6 },
            new Jugador { nombre = "Manuel", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("El ganador es: Pablo con 1", resultado);
        }
        [Test]
        public void Caso21()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 11 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 1 },
            new Jugador { nombre = "Felix", puntaje = 6 },
            new Jugador { nombre = "Manuel", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("Empate entre: Pablo con 1 y Miguel con 1", resultado);
        }
        [Test]
        public void Caso22()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 6 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 1 },
            new Jugador { nombre = "Felix", puntaje = 6 },
            new Jugador { nombre = "Manuel", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("Empate entre: Pablo con 1 y Miguel con 1", resultado);
        }
        [Test]
        public void Caso23()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 6 },
            new Jugador { nombre = "Pablo", puntaje = 1 },
            new Jugador { nombre = "Miguel", puntaje = 1 },
            new Jugador { nombre = "Felix", puntaje = 6 },
            new Jugador { nombre = "Manuel", puntaje = 6 },
            new Jugador { nombre = "Manuel", puntaje = 7 },
        };
            var paso = jugadores.ToArray();
            Assert.Throws(typeof(Exception), () => utils.Todo(paso));
        }
        [Test]
        public void Caso24()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 6 },
        };
            var paso = jugadores.ToArray();
            Assert.Throws(typeof(Exception), () => utils.Todo(paso));
        }
        [Test]
        public void Caso25()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 6 },
            new Jugador { nombre = "Pablo", puntaje = 11 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
            new Jugador { nombre = "Felix", puntaje = 3 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("El ganador es: Felix con 3", resultado);
        }
        [Test]
        public void Caso26()
        {
            var utils = new PokerGame();
            List<Jugador> jugadores = new List<Jugador>() { new Jugador { nombre = "Pedro", puntaje = 7 },
            new Jugador { nombre = "Pablo", puntaje = 11 },
            new Jugador { nombre = "Miguel", puntaje = 7 },
        };
            var paso = jugadores.ToArray();
            var resultado = utils.Todo(paso);
            Assert.AreEqual("Empate entre: Pedro con 7 y Miguel con 7", resultado);
        }
        [Test]
        public void Caso27()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso28()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso29()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
        [Test]
        public void Caso30()
        {
            var utils = new PokerGame();
            var resultado = utils.VerificarJugadores(3);
            Assert.AreEqual("Los jugadores son los adecuados", resultado);
        }
    }
}
